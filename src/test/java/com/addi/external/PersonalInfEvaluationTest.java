package com.addi.external;

import com.addi.domain.Lead;
import com.addi.domain.Person;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test for Personal Information evaluation")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PersonalInfEvaluationTest {

    private Evaluable underTest;

    @BeforeAll
    void setUp() {
        underTest = new PersonalInfEvaluation();
    }

    @Test
    @DisplayName("Should return true")
    void shouldBeValidPerson() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "123459",
                "e@e.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isTrue();

    }

    @Test
    @DisplayName("Should return false")
    void shouldBeInvalidPerson() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "123459",
                "e@exxxxx.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isFalse();

    }

    @Test
    @DisplayName("Should return false because person no exist")
    void shouldBeFalseWhenPersonIsNotInDb() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "11111111",
                "e@exxxxx.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isFalse();

    }
}