package com.addi.external;

import com.addi.domain.Lead;
import com.addi.domain.Person;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test for Judiciary evaluation")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JudicialEvaluationTest {

    private Evaluable underTest;

    @BeforeAll
    void setUp() {
        underTest = new JudicialEvaluation();
    }

    @Test
    @DisplayName("The Person doesn't have judiciary records")
    void shouldNotHaveJudiciaryRecords() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "123459",
                "e@e.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isFalse();

    }

    @Test
    @DisplayName("The Person have judiciary records")
    void shouldHaveJudiciaryRecords() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "123456",
                "e@e.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isTrue();

    }

    @Test
    @DisplayName("The Person doesn't exist")
    void shouldBeFalse() throws ExecutionException, InterruptedException {
        Person validPerson = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "xxxxxxx",
                "e@e.com");
        var lead = new Lead(validPerson);
        var future = underTest.eval(lead);
        assertThat(future.get()).isFalse();

    }
}