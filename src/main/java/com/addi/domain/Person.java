package com.addi.domain;

import java.time.LocalDate;
import java.util.UUID;

public class Person {

    private final UUID id;
    private final String firstName;
    private final String lastName;
    private final LocalDate birthdate;
    private final String dni;
    private final String email;

    public Person(String firstName, String lastName, LocalDate birthdate, String dni, String email) {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.dni = dni;
        this.email = email;
    }

    public static Person defaultPerson() {
        return new Person("default",
                "default",
                LocalDate.now(),
                "default",
                "default");
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public String getDni() {
        return dni;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthdate=" + birthdate +
                ", dni='" + dni + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!firstName.equals(person.firstName)) return false;
        if (!lastName.equals(person.lastName)) return false;
        if (!birthdate.equals(person.birthdate)) return false;
        if (!dni.equals(person.dni)) return false;
        return email.equals(person.email);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + birthdate.hashCode();
        result = 31 * result + dni.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
