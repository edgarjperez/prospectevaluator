package com.addi.domain;

import java.time.Instant;
import java.util.UUID;

public class Prospect {

    private final UUID id;
    private final Person person;
    private final Instant dateCreated;

    public Prospect(Person person) {
        this.id = UUID.randomUUID();
        this.person = person;
        this.dateCreated = Instant.now();
    }

    public UUID getId() {
        return id;
    }

    public Person getPerson() {
        return person;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "Prospect{" +
                "id=" + id +
                ", person=" + person +
                ", dateCreated=" + dateCreated +
                '}';
    }
}
