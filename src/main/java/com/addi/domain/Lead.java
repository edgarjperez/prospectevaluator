package com.addi.domain;

import java.time.Instant;

import java.util.UUID;

public class Lead {

    private final UUID id;
    private final Person person;
    private final Instant dateCreated;

    public Lead(Person person) {
        this.id = UUID.randomUUID();
        this.person = person;
        this.dateCreated = Instant.now();
    }

    public UUID getId() {
        return id;
    }

    public Person getPerson() {
        return person;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "Lead{" +
                "id=" + id +
                ", person=" + person +
                ", dateCreated=" + dateCreated +
                '}';
    }
}
