package com.addi.main;

import com.addi.domain.Lead;
import com.addi.domain.Person;
import com.addi.external.Evaluable;
import com.addi.external.JudicialEvaluation;
import com.addi.external.PersonalInfEvaluation;
import com.addi.internal.ProcessLeadService;
import com.addi.internal.ProspectQualificationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.concurrent.ExecutionException;


public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class.getName());


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Person p = new Person("Edgar",
                "Perez",
                LocalDate.of(1986, 4, 26),
                "123459",
                "e@e.com");
        Lead lead = new Lead(p);
        var service = new ProcessLeadService(
                new PersonalInfEvaluation(),
                new JudicialEvaluation(),
                new ProspectQualificationImpl());

        logger.info("Starting Validation process for lead {}", lead);
        service.process(lead);

    }
}
