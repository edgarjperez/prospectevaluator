package com.addi.internal;

import com.addi.domain.Lead;
import com.addi.domain.Prospect;
import com.addi.external.JudicialEvaluation;
import com.addi.external.PersonalInfEvaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class ProcessLeadService {

    private static Logger logger = LoggerFactory.getLogger(ProcessLeadService.class.getName());

    private final PersonalInfEvaluation personalInfEvaluation;
    private final JudicialEvaluation judicialEvaluation;
    private final ProspectQualification prospectQualification;


    public ProcessLeadService(PersonalInfEvaluation personalInfEvaluation,
                              JudicialEvaluation judicialEvaluation,
                              ProspectQualification prospectQualification) {
        this.personalInfEvaluation = personalInfEvaluation;
        this.judicialEvaluation = judicialEvaluation;
        this.prospectQualification = prospectQualification;
    }

    public void process(Lead lead) throws ExecutionException, InterruptedException {
        var personalInformationFuture =
                supplyAsync(() -> personalInfEvaluation.eval(lead));
        var judicialEvalFuture =
                supplyAsync(() -> judicialEvaluation.eval(lead));

        var completed = CompletableFuture.allOf(personalInformationFuture, judicialEvalFuture);

        try {
            completed.get();
        } catch (ExecutionException | InterruptedException e) {
            logger.error("It was an error", e);
        }
        logger.info("The result of personal information validation was {}", personalInformationFuture.get().get());
        logger.info("The result of judicial validation was {}", judicialEvalFuture.get().get());
        if (isPassValidations(personalInformationFuture, judicialEvalFuture)) {

            var score = prospectQualification.getScore(lead);
            logger.info("Score obtained: {}", score);
            if (score > 60) {
                var prospect = new Prospect(lead.getPerson());
                logger.info("The following prospect was generated: {}", prospect);
            } else {
                logger.error("The Lead {} Doesn't Qualify", lead);
            }
        }
    }

    private boolean isPassValidations(CompletableFuture<Future<Boolean>> personalInformationFuture,
                                      CompletableFuture<Future<Boolean>> judicialEvalFuture)
            throws InterruptedException, ExecutionException {
        return personalInformationFuture.get().get() && !judicialEvalFuture.get().get();
    }
}
