package com.addi.internal;

import com.addi.domain.Lead;

import java.util.Random;

public class ProspectQualificationImpl implements ProspectQualification {
    private static final int MAX_SCORE = 100;

    @Override
    public int getScore(Lead lead) {
        return new Random().nextInt(MAX_SCORE + 1); // Plus one because the upper bound is exclusive
    }
}
