package com.addi.internal;

import com.addi.domain.Lead;

public interface ProspectQualification {
    int getScore(Lead lead);
}
