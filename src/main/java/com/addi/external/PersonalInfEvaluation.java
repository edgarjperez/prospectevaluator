package com.addi.external;

import com.addi.domain.Lead;
import com.addi.domain.Person;
import com.addi.internal.ProcessLeadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class PersonalInfEvaluation implements Evaluable {
    private static Logger logger = LoggerFactory.getLogger(PersonalInfEvaluation.class.getName());

    private final Map<String,
            Person> persons = Map.of("123456",
            new Person("John",
                    "Doe",
                    LocalDate.of(1960, 4, 26),
                    "123456",
                    "john@mailinator.com"
            ), "123457",
            new Person("Ethan",
                    "Marcotte",
                    LocalDate.of(1970, 4, 26),
                    "123457",
                    "ethan@mailinator.com"
            ), "123458",
            new Person("Josh",
                    "Long",
                    LocalDate.of(1980, 4, 26),
                    "123458",
                    "josh@mailinator.com"
            ), "123459",
            new Person("Edgar",
                    "Perez",
                    LocalDate.of(1986, 4, 26),
                    "123459",
                    "e@e.com")
    );

    @Override
    public Future<Boolean> eval(Lead lead) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(2));
            logger.info("Running personal information validation for lead {}", lead);
            return CompletableFuture.completedFuture(isValid(lead.getPerson()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isValid(Person person) {
        return persons.getOrDefault(person.getDni(), Person.defaultPerson()).equals(person);
    }
}
