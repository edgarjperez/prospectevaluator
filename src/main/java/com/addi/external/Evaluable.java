package com.addi.external;

import com.addi.domain.Lead;

import java.util.concurrent.Future;

public interface Evaluable {

    Future<Boolean> eval(Lead lead);
}
