package com.addi.external;

import com.addi.domain.Lead;
import com.addi.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class JudicialEvaluation implements Evaluable {
    private static Logger logger = LoggerFactory.getLogger(PersonalInfEvaluation.class.getName());

    private final Map<String, Boolean> judiciaryRecords = Map.of(
            "123456", true,
            "123457", false,
            "123458", true,
            "123459", false
    );

    @Override
    public Future<Boolean> eval(Lead lead) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(2));
            logger.info("Running judicial validation for lead {}", lead);
            return CompletableFuture.completedFuture(hasJudiciaryRecords(lead.getPerson()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean hasJudiciaryRecords(Person person) {
        return judiciaryRecords.getOrDefault(person.getDni(), false);
    }
}
