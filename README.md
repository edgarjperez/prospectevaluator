# Prospect Evaluator

# Approach

I started to determine the principal actors and actions taken by these,
the following domain classes emerged:

- *Person*, Represent the inmutable object that holds the information of a Person.
- *Lead*, Represent the business case, that holds a Person object and the relevant information for logging process.
- *Prospect*, contains the Person in case it was approved.

## Technical Considerations

I divided the system in two principal packages *Internal* and *External*:

- *Internal*, Is where the system executes the qualification of a Lead and call external validations on it.
- *External*, In this package are located the API's of external systems, this external have their own repository od data for simplicity.

## Assumptions

- I took for granted that the external API's have their own repository of Data.
- I don't provide an API to connect with the sales system and receive incoming request for lead validation.
- The solution don't have a 100% of test coverage, the *ProcessLeadService* is not covered by tests.

## Improvements

- Achieve 100% of test coverage
- For the Prospect qualification is received a Lead object that's not used. Probably we can use the Lead information in order to perfor the qualification